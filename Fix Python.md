# Python Path Problems
First identify which version of python you are using:

`which python` 

`python` is symlinked to `python3`

/usr/bin/python -> /usr/bin/python3 

`python3` is symlinked to `python3.9`

/usr/bin/python3 -> /usr/bin/python3.9


`which python3`= /usr/local/bin/python3


I'd recommend using an absolute path to a specific version.


# Create Virtual environment
```
sudo apt-install python3-venv
python -m venv <target dir>
```
This creates a copy of python in the target directory.

# Activate the virtual environment
```
source <target dir>/bin/activate
```
Now when I use the command `which python` it will display `<target dir>/python`

# Install packages into the venv.

I can use the virtual-env's pip to install packages.

`pip install -r requirements.txt`  

or for Impacket.. `pip install impacket`

Now these packages wont interact with the system's versions of python. It keeps the impacket scripts contained, and doesnt break the system ones.

# Deactivate the venv

Exit the shell. CTRL+D or simply close the window

To re-enter the virtual environment, source the `<dir>/bin/activate` script again.





